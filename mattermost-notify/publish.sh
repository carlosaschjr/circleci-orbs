circleci orb validate orb.yml
VERSION=$(cat version | sed -e 's/[[:space:]]*$//')
circleci orb publish orb.yml tokenlab/mattermost-notify@$VERSION
